// @flow

import {CALL_API} from '../middleware/api'
import * as types from '../constants/ActionTypes'

/**
 * Action creator for getting information about logged user.
 *
 * @returns {function(Function, Function)}
 */
export const getLoggedUser = () => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.LOGIN_USER_DETAILS_REQUEST, types.LOGIN_USER_DETAILS_SUCCESS, types.LOGIN_USER_DETAILS_FAILURE],
			endpoint: `/auth/me`,
			method: 'GET',
			token: getState().user.token,
		},
	});
};

/**
 * Action creator for updating information about logged user.
 *
 * @param data Object
 * @returns {function(Function, Function)}
 */
export const updateLoggedUser = (data: Object) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.UPDATE_LOGIN_USER_DETAILS_REQUEST, types.UPDATE_LOGIN_USER_DETAILS_SUCCESS, types.UPDATE_LOGIN_USER_DETAILS_FAILURE],
			endpoint: `/auth/me`,
			method: 'POST',
			data: data,
			token: getState().user.token,
		},
	})
};

/**
 * Action creator for log user out.
 *
 * @returns {function(Function, Function)}
 */
export const logoutUser = () => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.LOGOUT_REQUEST, types.LOGOUT_SUCCESS, types.LOGOUT_FAILURE],
			endpoint: `/auth/logout`,
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const getRepositories = (reload: boolean) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_REPOSITORIES_REQUEST, types.GET_REPOSITORIES_SUCCESS, types.GET_REPOSITORIES_FAILURE],
			endpoint: `/repositories` + (reload ? '?reload' : ''),
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const registerWebHook = (repositoryId: number) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.POST_REPOSITORIES_REQUEST, types.POST_REPOSITORIES_SUCCESS, types.POST_REPOSITORIES_FAILURE],
			endpoint: `/repositories/${repositoryId}`,
			method: 'POST',
			token: getState().user.token,
		},
	});
};

export const getBranches = (repositoryId: number) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_BRANCHES_REQUEST, types.GET_BRANCHES_SUCCESS, types.GET_BRANCHES_FAILURE],
			endpoint: `/branches/${repositoryId}`,
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const getCommits = (branchId: string, lastTime: ?string) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_COMMITS_REQUEST, types.GET_COMMITS_SUCCESS, types.GET_COMMITS_FAILURE],
			endpoint: `/commits/${branchId}` + (lastTime ? '?lastTime=' + lastTime : ''),
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const getCommitDetail = (commitHash: string, vcs: number, onlyNew: ?boolean) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_COMMIT_REQUEST, types.GET_COMMIT_SUCCESS, types.GET_COMMIT_FAILURE],
			endpoint: `/commit/${commitHash}?vcs=` + vcs + (onlyNew ? '&onlyNew=1' : ''),
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const getCommitCommonIssues = (commitHash: string, vcs: number, onlyNew: ?boolean, limit: ?number) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_COMMIT_COMMON_ISSUES_REQUEST, types.GET_COMMIT_COMMON_ISSUES_SUCCESS, types.GET_COMMIT_COMMON_ISSUES_FAILURE],
			endpoint: `/commit/${commitHash}/common-issues?vcs=${vcs}` + (onlyNew ? '&onlyNew=1' : '') + (limit ? '&limit=' + limit : ''),
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const getCommitFileIssues = (fileId: number, commitHash: string, vcs: number, onlyNew: ?boolean) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_COMMIT_FILE_ISSUES_REQUEST, types.GET_COMMIT_FILE_ISSUES_SUCCESS, types.GET_COMMIT_FILE_ISSUES_FAILURE],
			endpoint: `/commit/${commitHash}/issues/${fileId}?vcs=${vcs}` + (onlyNew ? '&onlyNew=1' : ''),
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const getPullRequests = (repositoryId: number) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_PULL_REQUESTS_REQUEST, types.GET_PULL_REQUESTS_SUCCESS, types.GET_PULL_REQUESTS_FAILURE],
			endpoint: `/pull-requests/${repositoryId}`,
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const getPullRequest = (repositoryId: number, id: number) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_PULL_REQUEST_REQUEST, types.GET_PULL_REQUEST_SUCCESS, types.GET_PULL_REQUEST_FAILURE],
			endpoint: `/pull-requests/${repositoryId}/${id}`,
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const getOrganizations = () => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_ORGANIZATIONS_REQUEST, types.GET_ORGANIZATIONS_SUCCESS, types.GET_ORGANIZATIONS_FAILURE],
			endpoint: `/organizations`,
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const getOrganizationDetail = (name: string) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_ORGANIZATION_DETAIL_REQUEST, types.GET_ORGANIZATION_DETAIL_SUCCESS, types.GET_ORGANIZATION_DETAIL_FAILURE],
			endpoint: `/organizations/${name}`,
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const addOrganization = (data: Object) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.ADD_ORGANIZATION_REQUEST, types.ADD_ORGANIZATION_SUCCESS, types.ADD_ORGANIZATION_FAILURE],
			endpoint: `/organizations`,
			method: 'POST',
			data: data,
			token: getState().user.token,
		},
	})
};

export const updateOrganization = (name: string, data: Object) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.UPDATE_ORGANIZATION_REQUEST, types.UPDATE_ORGANIZATION_SUCCESS, types.UPDATE_ORGANIZATION_FAILURE],
			endpoint: `/organizations/${name}`,
			method: 'PUT',
			data: data,
			token: getState().user.token,
		},
	})
};

export const addOrganizationMember = (name: string, email: string) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.ADD_ORGANIZATION_MEMBER_REQUEST, types.ADD_ORGANIZATION_MEMBER_SUCCESS, types.ADD_ORGANIZATION_MEMBER_FAILURE],
			endpoint: `/organizations/${name}/members`,
			method: 'POST',
			data: {
				email: email,
			},
			token: getState().user.token,
		},
	})
};

export const removeOrganizationMember = (name: string, userId: number) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.REMOVE_ORGANIZATION_MEMBER_REQUEST, types.REMOVE_ORGANIZATION_MEMBER_SUCCESS, types.REMOVE_ORGANIZATION_MEMBER_FAILURE],
			endpoint: `/organizations/${name}/members/${userId}`,
			method: 'DELETE',
			token: getState().user.token,
		},
	})
};

export const finishInvitation = (token: string) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.FINISH_INVITATION_REQUEST, types.FINISH_INVITATION_SUCCESS, types.FINISH_INVITATION_FAILURE],
			endpoint: `/organizations/invitations`,
			method: 'POST',
			data: {
				token: token,
			},
			token: getState().user.token,
		},
	})
};

export const removeOrganizationInvite = (name: string, email: string) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.REMOVE_ORGANIZATION_INVITE_REQUEST, types.REMOVE_ORGANIZATION_INVITE_SUCCESS, types.REMOVE_ORGANIZATION_INVITE_FAILURE],
			endpoint: `/organizations/${name}/invitations`,
			method: 'DELETE',
			data: {
				email: email,
			},
			token: getState().user.token,
		},
	})
};

export const getBillingPlans = () => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_BILLING_PLANS_REQUEST, types.GET_BILLING_PLANS_SUCCESS, types.GET_BILLING_PLANS_FAILURE],
			endpoint: `/billing/plans`,
			method: 'GET',
			token: getState().user.token,
		},
	});
};

export const changeBillingPlan = (planId: number, annuallyPayment: boolean) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.CHANGE_BILLING_PLAN_REQUEST, types.CHANGE_BILLING_PLAN_SUCCESS, types.CHANGE_BILLING_PLAN_FAILURE],
			endpoint: `/auth/billing/plan`,
			method: 'POST',
			data: {
				planId: planId,
				annuallyPayment: annuallyPayment,
			},
			token: getState().user.token,
		},
	});
};

export const payPlan = (annuallyPayment: boolean) => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.PAY_PLAN_REQUEST, types.PAY_PLAN_SUCCESS, types.PAY_PLAN_FAILURE],
			endpoint: `/auth/billing/plan/pay`,
			method: 'POST',
			data: {
				annuallyPayment: annuallyPayment,
			},
			token: getState().user.token,
		},
	});
};

export const activateTrial = () => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.ACTIVATE_TRIAL_REQUEST, types.ACTIVATE_TRIAL_SUCCESS, types.ACTIVATE_TRIAL_FAILURE],
			endpoint: `/auth/billing/trial`,
			method: 'POST',
			token: getState().user.token,
		},
	});
};

export const getInvoices = () => (dispatch: Function, getState: Function) => {
	return dispatch({
		[CALL_API]: {
			types: [types.GET_INVOICES_REQUEST, types.GET_INVOICES_SUCCESS, types.GET_INVOICES_FAILURE],
			endpoint: `/billing/invoices`,
			method: 'GET',
			token: getState().user.token,
		},
	});
};
