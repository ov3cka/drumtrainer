jQuery(document).ready(function($){

    $availablePoints = $('.points-all-value').html();

    function redirect_to_points() {
        if (document.documentElement.lang === 'en-US') {
            window.location.href = '/en/points';
        }
        else {
            window.location.href = '/body';
        }
    }

    function unlockLoop(postId, $points) {

        if($availablePoints >= $points) {
            $.ajax({
                beforeSend: function() {
                },
                url: points.ajax_url,
                type: "post",
                async: "false",
                data: {
                    display: 1,
                    action: 'points',
                    postId: postId,
                    type: 'loop'
                },
                complete: function(){
                    loadLoops(postId);

                },
                success: function (response) {
                    let display = $(".loop-" + postId);
                    display.html(response);
                    recountSubmenuUnlockedLoops();
                    reloadPoints($points);
                }
            });
        }
        else {
            redirect_to_points();
        }
    }

    function unlockVideo(postId, $points = 0) {

        if($availablePoints >= $points) {
            $.ajax({
                beforeSend: function() {
                },
                url: points.ajax_url,
                type: "post",
                async: "false",
                data: {
                    display: 1,
                    action: 'points',
                    postId: postId,
                    type: 'video'
                },
                complete: function(){
                },
                success: function (response) {
                    let display = $("#post-" + postId + " .vimeo-content");
                    display.html(response);
                    recountSubmenuUnlockedLoops();
                    reloadPoints($points);
                }
            });
        }
        else {
            redirect_to_points();
        }
    }

    $(document).on("click", ".controls-unlock", function (e) {
        e.preventDefault();
        let id = this.id.replace('post-', '');
        let videoPoints = parseInt($(this).find('.video-points .points-value').html()) || 0;
        let loopPoints = parseInt($(this).find('.controls-unlock-loop .points-value').html()) || 0;

        if(this.classList.contains('controls-unlock-loop')) {
            unlockLoop(id, loopPoints);
        }
        else {
            unlockVideo(id, videoPoints);
        }
    });

    function loadLoops(postId) {
        var loops = document.querySelectorAll('.loop-item .loop-' + postId);

        for (var i = 0; i < loops.length; i++) {
            var currentLoop = loops[i];

            var play = currentLoop.querySelector('.controls-play');
            play.addEventListener("click", function (e) {
                e.preventDefault();
                var parent = this.parentNode.parentNode;
                var iframe = parent.querySelector('.loop-content iframe');
                var widget = SC.Widget(iframe);
                var icon = this.querySelector('.icon');


                widget.bind(SC.Widget.Events.READY, function () {
                    widget.isPaused(function isPaused(p) {
                        console.log('ispaused1 ', p);
                        if (p === true) {
                            icon.classList.replace('icon-play', 'icon-pause');

                        } else {
                            icon.classList.replace('icon-pause', 'icon-play');

                        }
                    });

                    widget.bind(SC.Widget.Events.PLAY, function () {
                        console.log('played');
                        widget.isPaused(function isPaused(p) {
                            console.log('ispaused2', p);

                            if (p === true) {
                                icon.classList.replace('icon-play', 'icon-pause');
                            } else {
                                icon.classList.replace('icon-pause', 'icon-play');

                            }
                        });

                    });
                });
            });

            var resume = currentLoop.querySelector('.controls-resume');
            resume.addEventListener('click', function (e) {
                e.preventDefault();

                var parent = this.parentNode;
                var iframe = parent.querySelector('iframe');
                var widget = SC.Widget(iframe);

                widget.bind(SC.Widget.Events.READY, function () {
                    widget.seekTo(0);
                })

            })
        }
    }

    function recountSubmenuUnlockedLoops() {
        let submenuItem = $('.submenu .selected .unlocked-count-all');
        let submenuItemCount = parseInt( submenuItem.html() ) || 0;
        submenuItem.html( submenuItemCount + 1 )
    }

    function reloadPoints(points) {
        let pointsElement = $('.points-all-value');
        let pointsElementValue =  parseInt( pointsElement.html() );
        pointsElement.html( pointsElementValue - points )
    }
});
