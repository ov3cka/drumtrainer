jQuery(document).ready(function($) {
    $totalHours = $('.total-hours-value').html() || 0;
    $cancelRewardButtons = document.querySelectorAll('.cancel-reward');

    $cancelRewardButtons.forEach(function (reward) {
        $(reward)[0].addEventListener('click', function (e) {
            e.preventDefault();
            let rewardId = this.id.replace("cancel-reward-", "");
            cancelReward(rewardId);
        });
    });

    function cancelReward(rewardId) {
        $.ajax({
            url: rewards.ajax_url,
            type: "post",
            async: "false",
            data: {
                display: 1,
                action: 'reward_cancel',
                rewardId: rewardId
            },
            success: function (response) {
                $rewardButtonWrapper.html(response);
                $('.total-hours-value').html(Number($totalHours) + 20);
            }
        });
    }

});
