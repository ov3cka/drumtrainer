(function ($) {

    $(document).on('click', '#em-booking-submit', function (e) {
        e.preventDefault();

        var $thisbutton = $(this),
            $form = $thisbutton.closest('form.em-booking-form'),
            id = $thisbutton.val(),
            product_qty = $form.find('#em-ticket-spaces-1').val() || 1,
            product_id = $form.find('input[name=product_id]').val() || 1643,
            variation_id = $form.find('input[name=variation_id]').val() || 0,
            cart_item_data = {'date':$('.post-content p')[0].innerText, 'comment':$form.find('.booking-comment').val() };

        var data = {
            action: 'woocommerce_ajax_add_to_cart',
            product_id: product_id,
            product_sku: '',
            quantity: product_qty,
            variation_id: variation_id,
            cart_item_data: cart_item_data
        };

        $(document.body).trigger('adding_to_cart', [$thisbutton, data]);

        $.ajax({
            type: 'post',
            url: wc_add_to_cart_params.ajax_url,
            data: data,
            beforeSend: function (response) {
                $thisbutton.removeClass('added').addClass('loading');
            },
            complete: function (response) {
                $thisbutton.addClass('added').removeClass('loading');
            },
            success: function (response) {

                if (response.error & response.product_url) {
                    window.location = response.product_url;
                    return;
                } else {
                    $(document.body).trigger('added_to_cart', [response.fragments, response.cart_hash, $thisbutton]);
                }
            },
        });

        $('form[name="booking-form"]').submit();

        return false;
    });
})(jQuery);