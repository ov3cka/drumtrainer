jQuery(document).ready(function($) {

    $sendRewardButtons = document.querySelectorAll('.send-reward');

    $sendRewardButtons.forEach(function (reward) {
        $(reward)[0].addEventListener('click', function (e) {
            e.preventDefault();
            let rewardId = this.id.replace("send-reward-", "");
            $userId = this.dataset.userid;
            if($userId) {
                approveReward(rewardId, $userId);
            }
            
        })
    });

    function approveReward(rewardId, userId) {
        let display = $('.users-rewards');
        $.ajax({
            url: admin_rewards.ajax_url,
            type: "post",
            async: "false",
            data: {
                display: 1,
                action: 'admin_rewards',
                approve: true,
                userId: userId,
                rewardId: rewardId
            },
            complete: function () {

            },
            success: function (response) {
                display.html(response);
            }
        });
    }

    hookCancelButtons();

    function hookCancelButtons() {
        $cancelRewardButtons = document.querySelectorAll('.cancel-reward');

        $cancelRewardButtons.forEach(function (reward) {
            $(reward)[0].addEventListener('click', function (e) {
                e.preventDefault();
                let rewardId = this.id.replace("cancel-reward-", "");
                $rewardHours = this.dataset.rewardhours;
                $userId = this.dataset.userid;
                cancelReward(rewardId, this, $rewardHours, $userId);
            });
        });
    }

    function cancelReward(rewardId, element, $rewardHours, $userId) {
        let display = $('.users-rewards');
        $rewardButtonWrapper = $(element.parentElement);
        $rewardImgOverlay = $('#product-reward-' + rewardId + ' .image-overlay')[0];

        $.ajax({
            url: admin_rewards.ajax_url,
            type: "post",
            async: "false",
            data: {
                display: 1,
                action: 'admin_reward_cancel',
                rewardId: rewardId,
                userId: $userId,
                rewardHours: $rewardHours
            },
            success: function (response) {
                display.html(response);
            }
        });
    }
});
