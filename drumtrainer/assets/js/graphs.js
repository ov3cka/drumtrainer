function drawCharts() {

    var data = document.querySelector('.graph-data-systems');
    if(data) {
        data = JSON.parse(data.innerHTML);

        var date = [];
        var system_a = [];
        var system_b = [];
        var system_c = [];
        var system_d = [];
        var total_time = [];

        for (var i in data) {
            var dateFormatted = new Date(data[i].date);
            date.push(dateFormatted.toLocaleDateString());
            system_a.push(data[i].a_system);
            system_b.push(data[i].b_system);
            system_c.push(data[i].c_system);
            system_d.push(data[i].d_system);
            total_time.push(data[i].total_time);
        }

        var chartdata = {
            labels: date,
            datasets: [
                {
                    label: 'A system',
                    data: system_a,
                    borderColor: 'red',
                    hoverBorderColor: 'rgba(200, 200, 200, 1)',
                    borderWidth: 1,
                    fill: false,
                },
                {
                    label: 'B system',
                    data: system_b,
                    borderColor: 'blue',
                    hoverBorderColor: 'rgba(200, 200, 200, 1)',
                    borderWidth: 1,
                    fill: false,
                },
                {
                    label: 'C system',
                    data: system_c,
                    borderColor: 'green',
                    hoverBorderColor: 'rgba(200, 200, 200, 1)',
                    borderWidth: 1,
                    fill: false,
                },
                {
                    label: 'D system',
                    data: system_d,
                    borderColor: 'yellow',
                    hoverBorderColor: 'rgba(200, 200, 200, 1)',
                    borderWidth: 1,
                    fill: false,
                }
            ]
        };

        var graphSystemsElement = document.getElementById('chartSystems');
        new Chart(graphSystemsElement, {
            type: 'line',
            data: chartdata,
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Přehled odcvičených minut podle systémů'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Datum'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Minuty'
                        }
                    }]
                }
            }
        });

        var chartdataTotalTime = {

            labels: date,
            datasets: [
                {
                    label: 'Čas',
                    data: total_time,
                    borderColor: 'red',
                    hoverBorderColor: 'rgba(200, 200, 200, 1)',
                    borderWidth: 1,
                    fill: false,
                }
            ]
        };

        var graphTotalTimeElement = document.getElementById('chartTotalTime');

        new Chart(graphTotalTimeElement, {
            type: 'line',
            data: chartdataTotalTime,
            options: {
                responsive: true,
                title: {
                    display: true,
                    text: 'Přehled odcvičených minut'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Datum'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Minuty'
                        }
                    }]
                }
            }
        });

        let group = document.querySelector('.radio-group');
        let radioSystems = document.querySelector('#systems-radio');
        let radioTotalTime = document.querySelector('#total-time-radio');

        graphSystemsElement.style.display = "none";
        radioTotalTime.checked = true;

        group.addEventListener('click', function () {
            if (radioSystems.checked) {
                graphTotalTimeElement.style.display = "none";
                graphSystemsElement.style.display = "block";
            }
            if (radioTotalTime.checked) {
                graphSystemsElement.style.display = "none";
                graphTotalTimeElement.style.display = "block";
            }

        });
    }
};
function destroyCharts() {

    jQuery(document).ready(function(){
        var graphSystemsElement = document.getElementById('chartSystems');
        var graphTotalTimeElement = document.getElementById('chartTotalTime');

        var chartsElement = document.querySelector('.charts');

        graphSystemsElement.remove();
        graphTotalTimeElement.remove();

        var graph1 = document.createElement('canvas');
        graph1.id = 'chartTotalTime';
        var graph2 = document.createElement('canvas');
        graph2.id = 'chartSystems';

        chartsElement.appendChild(graph1);
        chartsElement.appendChild(graph2);
    });
}
