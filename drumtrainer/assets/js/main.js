function getSystemTotalTime(system) {
    let inputs = system.querySelectorAll('.input-time');
    let totalTime = 0;

    for (let i = 0; i < inputs.length; i++) {
        if(parseInt(inputs[i].value))
        totalTime += parseInt(inputs[i].value);
	}

	let spanTime = system.querySelector('span.total-time');
	spanTime.innerHTML = totalTime + ' minut';

	getTotalTime();

	return totalTime;
}

function getTotalTime()
{
    let inputs = document.querySelectorAll('.input-time');
    let totalTime = 0;
    for (let i = 0; i < inputs.length; i++) {
        if(parseInt(inputs[i].value)) {
			totalTime += parseInt(inputs[i].value);
		}
	}

	let spanTime = document.querySelector('div.total-time span');
	let inputTime = document.querySelector('#training_form_plan_total_time');
	spanTime.innerHTML = totalTime + ' minut';
	inputTime.value = totalTime;

	return totalTime;
}


( function() {
    let systems = document.querySelectorAll('.system-box');
    for(let s = 0; s < systems.length; s++) {
		let addNewButtons = document.querySelectorAll('.add-new');
		let inputs = systems[s].querySelectorAll('.input-time');

		for (let i = 0; i < inputs.length; i++) {
			inputs[i].addEventListener('blur', function () {
				getSystemTotalTime(systems[s]);
			});
		}
		for (let j = 0; j < addNewButtons.length; j++) {
			addNewButtons[j].addEventListener('click', function () {
				inputs = systems[s].querySelectorAll('.input-time');
				for (let i = 0; i < inputs.length; i++) {
					inputs[i].addEventListener('blur', function () {
						getSystemTotalTime(systems[s]);
					});
				}
			});
		}
	}

    let testimonialsWrapper = document.querySelector('.testimonials');

	jQuery(document).ready(function($) {
		let imgPath = main.templateurl + "/assets/img/";

		$('.testimonials').slick({
			infinite: true,
			speed: 2000,
			slidesToShow: 3,
			slidesToScroll: 1,
			nextArrow: '<button class="slide-arrow next-arrow"><img src="' + imgPath + 'arrow_right.svg"></button>',
			prevArrow: '<button class="slide-arrow prev-arrow"><img src="' + imgPath + 'arrow_left.svg"></button>',
			autoplay: 'true',
			responsive: [
				{
					breakpoint: 1600,
					settings: {
						slidesToShow: 3,
						slidesToScroll: 1,
					}
				},
				{
					breakpoint: 1024,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
					}
				},
				{
					breakpoint: 600,
					settings: {
						slidesToShow: 2,
						slidesToScroll: 2
					}
				},
				{
					breakpoint: 480,
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1
					}
				}
			]
		});
		if(testimonialsWrapper) {
			testimonialsWrapper.style.display = "block";
		}
	});

})();

function addTrainingFormRow(className, name) {
	let planInputs = document.querySelectorAll('.' + className + ' .system-input');
	let i = planInputs.length;

    let addNewButtons = document.querySelectorAll('.add-new');
    let template = `
        <input id="training_form_systems_[${className}][${i}][name]" name="training_form_systems_[${className}][${i}][name]" type="hidden" value="${name}" class="system-input">
        <div class="col-sm-8 no-padding">
            <label for="training_form_systems_[${className}][${i}][exercise]">Cvičení</label>
            <input type="text" id="training_form_systems_[${className}][${i}][exercise]" name="training_form_systems_[${className}][${i}][exercise]" class="form-control first-control" value="" />
        </div>
        <div class="col-sm-2 no-padding">
			<label for="training_form_systems_[${className}][${i}][bmp]">BMP</label>
			<input type="number" id="training_form_systems_[${className}][${i}][bmp]" name="training_form_systems_[${className}][${i}][bmp]" class="form-control last-control" value="" />
		</div>
        <div class="col-sm-2 no-padding">
            <label for="training_form_systems_[${className}][${i}][time]">Čas</label>
            <input type="number" id="training_form_systems_[${className}][${i}][time]" name="training_form_systems_[${className}][${i}][time]" class="form-control last-control input-time" value="" />
        </div>
    `;

    let container = document.querySelector('div.' + className);
    let div = document.createElement('div');
    div.innerHTML = template;
    div.classList.add('form-group', 'row');
    container.appendChild(div);
}
