jQuery(document).ready(function($) {
    hookRewardsButtons();
    hookCancelButtons();

    function getTotalHours() {
        $totalHours = $('.total-hours-value').html() || 0;
        return $totalHours;
    }

    function hookRewardsButtons() {
        let rewardElements = document.querySelectorAll('.product-reward');
        rewardElements.forEach(function (reward) {
            $(reward).find('.get-reward')[0].addEventListener('click', function (e) {
                e.preventDefault();
                $rewardElement = this;
                $rewardHours = $rewardElement.dataset.rewardhours;
                if (Number(getTotalHours()) >= Number($rewardHours)) {
                    let rewardId = $rewardElement.id.replace("get-reward-", "");
                    getReward($rewardElement, rewardId, $rewardHours);
                }
                else {
                    $oldText = $($rewardElement).text();
                    $($rewardElement).html(rewards.too_little_points);
                    setTimeout(function(){
                        $($rewardElement).html($oldText);
                    }, 2000); 
                }
            })
        });
    }

    function getReward(element, rewardId, $rewardHours) {
        $rewardButtonWrapper = $(element.parentElement);
        $rewardWrapper = $('#product-reward-' + rewardId)[0];
        $rewardImgWrapper = $('#product-reward-' + rewardId + ' .reward-image-wrapper')[0];
        $rewardImgOverlay = document.createElement('DIV');
        $rewardImgOverlay.classList.add('image-overlay');

        $.ajax({
            url: rewards.ajax_url,
            type: "post",
            async: "false",
            data: {
                display: 1,
                action: 'rewards',
                reward: true,
                rewardId: rewardId
            },
            success: function (response) {
                $rewardButtonWrapper.html(response);
                $rewardImgWrapper.appendChild($rewardImgOverlay);
                $rewardWrapper.classList.add('taken');
                $('.total-hours-value').html(Number(getTotalHours()) - Number($rewardHours));
                hookCancelButtons();
            }
        });
    }

    function hookCancelButtons() {
        $cancelRewardButtons = document.querySelectorAll('.cancel-reward');

        $cancelRewardButtons.forEach(function (reward) {
            $(reward)[0].addEventListener('click', function (e) {
                e.preventDefault();
                let rewardId = this.id.replace("cancel-reward-", "");
                $rewardHours = this.dataset.rewardhours;
                cancelReward(rewardId, this, $rewardHours);
            });
        });
    }

    function cancelReward(rewardId, element, $rewardHours) {

        $rewardButtonWrapper = $(element.parentElement);
        $rewardImgWrapper = $('#product-reward-' + rewardId + ' .reward-image-wrapper')[0];
        $rewardImgOverlay = $('#product-reward-' + rewardId + ' .image-overlay')[0];

        $.ajax({
            url: trackerdata.ajax_url,
            type: "post",
            async: "false",
            data: {
                display: 1,
                action: 'reward_cancel',
                rewardId: rewardId,
                reward: false,
                rewardHours: $rewardHours,
                cancelReward: true
            },
            success: function (response) {
                $rewardButtonWrapper.html(response);
                $rewardImgWrapper.removeChild($rewardImgOverlay);
                $('.total-hours-value').html(Number(getTotalHours()) + Number($rewardHours));
                hookRewardsButtons();
            }
        });
    }
});
