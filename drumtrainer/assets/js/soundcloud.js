jQuery(document).ready(function($) {


    var loops = document.querySelectorAll('.loop-item .loop');
    var widgets = [];

    function loadLoops() {
        for (var i = 0; i < loops.length; i++) {

            var currentLoop = loops[i];

            var parent = currentLoop.parentNode;
            var iframe = parent.querySelector('iframe');
            widgets.push(SC.Widget(iframe));

            var widget = widgets[i];
            var icon = currentLoop.querySelector('.icon');

            var play = currentLoop.querySelector('.controls-play');
            if (play) {

                play.addEventListener("click", function (e) {
                    e.preventDefault();
                    console.log("click");
                    //widget.toggle();
                    widget.play();
                });
            };

            widget.bind(SC.Widget.Events.READY, function() {
                console.log("Ready");
            });

            widget.bind(SC.Widget.Events.PLAY, function () {
                icon.classList.replace('icon-play', 'icon-pause');
            });

            widget.bind(SC.Widget.Events.PAUSE, function () {
                icon.classList.replace('icon-pause', 'icon-play');
            });

            widget.bind(SC.Widget.Events.LOAD_PROGRESS, function (o) {
                console.log(o);
            });

            widget.bind(SC.Widget.Events.PLAY_PROGRESS, function (o) {
                console.log(o);
            });




/*            var resume = currentLoop.querySelector('.controls-resume');
            if (resume) {
                resume.addEventListener('click', function (e) {
                    e.preventDefault();

                    var parent = this.parentNode;
                    var iframe = parent.querySelector('iframe');
                    var widget = SC.Widget(iframe);

                    widget.bind(SC.Widget.Events.READY, function () {
                        widget.seekTo(0);
                    })
                })
            }*/
        }
    }

    loadLoops();
});
