jQuery(document).ready(function($){
    let today = new Date();
    month = today.getMonth() + 1;
    year = today.getFullYear();
    var display = document.querySelector("#display");

    if(display !== null) {
        let monthDisplay = $(".month-display");
        let months = [];

        if (document.documentElement.lang === 'en-US'){
            months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
        }
        else {
            months = ["Leden", "Únor", "Březen", "Duben", "Květen", "Červen", "Červenec", "Srpen", "Září", "Říjen", "Listopad", "Prosinec"];
        }

        monthDisplay.html(months[month - 1] + ' ' + year);
        loadTrackerData(month, year, 0);

        $(document).on("click", ".tracker-prev", function (e) {
            e.preventDefault();

            if (month === 1) {
                month = 13;
                year -= 1;
            }
            month -= 1;

            loadTrackerData(month, year);
            monthDisplay.html(months[month - 1] + ' ' + year);

            return false;
        });

        $(document).on("click", ".tracker-next", function (e) {
            e.preventDefault();

            month += 1;
            if (month === 13) {
                month = 1;
                year += 1;
            }

            loadTrackerData(month, year);
            monthDisplay.html(months[month - 1] + ' ' + year);

            return false;
        });
    }

    function loadTrackerData(month, year, destroy = 1) {

        $.ajax({
            beforeSend: function() {
                $('.loader').show();
                $('.tracker-section').hide();
                $('.radio-group').hide();
                $('.charts').hide();
            },
            url: trackerdata.ajax_url,
            type: "post",
            async: "false",
            data: {
                display: 1,
                action: 'tracker_table',
                month: month,
                year: year
            },
            complete: function(){
                $('.loader').hide();
                $('.tracker-section').show();
                $('.radio-group').show();
                $('.charts').show();
                drawCharts();
            },
            success: function (response) {
                if(destroy === 1)
                {
                    destroyCharts();
                }
                display.innerHTML = '';
                display.innerHTML = response;
                recountStars();
            }
        });
    }


    let trackerForm = document.querySelector('.tracker-form');
    if(trackerForm) {

        let trackerButton = document.querySelector('#tracker_save');
        let errorWrapper = document.createElement('div');
        errorWrapper.classList.add('error-wrapper');
        trackerForm.appendChild(errorWrapper);

        let isError = false;

        trackerButton.addEventListener('click', function (e) {

            while (errorWrapper.firstChild) {
                errorWrapper.removeChild(errorWrapper.firstChild);
            }

            let inputs = {
                'required': {
                    'systemA': parseInt(document.querySelector('.tracker-form .tracker-system-A').value),
                    'systemB': parseInt(document.querySelector('.tracker-form .tracker-system-B').value),
                    'systemC': parseInt(document.querySelector('.tracker-form .tracker-system-C').value),
                    'systemD': parseInt(document.querySelector('.tracker-form .tracker-system-D').value),
                },
                'notrequired': {
                    'trackerEmotion1': document.querySelector('.tracker-form #tracker_emotion_1'),
                    'trackerEmotion2': document.querySelector('.tracker-form #tracker_emotion_2'),
                    'trackerEmotion3': document.querySelector('.tracker-form #tracker_emotion_3'),
                }
            };

            // let systemCount = (isNaN(inputs.required.systemA) ? 0 : inputs.required.systemA) + (isNaN(inputs.required.systemB) ? 0 : inputs.required.systemB) + (isNaN(inputs.required.systemC) ? 0 : inputs.required.systemC) + (isNaN(inputs.required.systemD) ? 0 : inputs.required.systemD);

            // if (systemCount > 120) {
            //     e.preventDefault();
            //     addErrorMessage(errorWrapper, 'Do systémů A,B,C,D můžeš napsat maximálně 120 minut na den.');
            // }
            exceededSystemLimit = false;

            for (var key in inputs.required) {
                if (isNaN(inputs.required[key])) {
                    isError = true;
                }
            }
            for (var key in inputs.required) {
                if(inputs.required[key] > 120) {
                    exceededSystemLimit = true;
                    break;
                }
            }

            if (isError) {
                e.preventDefault();
                addErrorMessage(errorWrapper, 'Je nutné vyplnit všechny pole.');
                isError = false;
            }
            if (exceededSystemLimit) {
                e.preventDefault();
                addErrorMessage(errorWrapper, 'Do systémů A,B,C,D můžeš napsat maximálně 120 minut na den.');

                exceededSystemLimit = false;
            }

            if (inputs.notrequired.trackerEmotion1.checked === false && inputs.notrequired.trackerEmotion2.checked === false && inputs.notrequired.trackerEmotion3.checked === false) {
                e.preventDefault();
                addErrorMessage(errorWrapper, 'Zapoměl si zdělit jak se cítíš.');
            }

        });

        function addErrorMessage(wrapper, message) {
            let errorMessage = document.createElement('p');
            errorMessage.classList.add('error-message');
            errorMessage.innerText = message;
            errorWrapper.appendChild(errorMessage);
        }

        function recountStars() {
            let stars = document.querySelectorAll('.star-box');
            let currentHoursElement = document.querySelector('.total-time');
            if(currentHoursElement) {
                let currentHours = parseInt(currentHoursElement.dataset.hours);
                resetStars();
                for (let key in stars) {
                    if (stars[key].dataset) {
                        if (currentHours > stars[key].dataset.minValue && currentHours < stars[key].dataset.maxValue) {
                            stars[key].querySelector('.star-overlay').style.height = 100 - ((100 * currentHours) / stars[key].dataset.maxValue) + '%';
                        } else if (currentHours >= stars[key].dataset.maxValue) {
                            stars[key].querySelector('.star-overlay').style.height = '0px';
                        }
                    }

                }
            }
        }
        function resetStars() {
            let stars = document.querySelectorAll('.star-box');
            for (let key in stars) {
                if (stars[key].dataset) {
                    stars[key].querySelector('.star-overlay').style.height = '100%';
                }
            }
        }
    }
});


