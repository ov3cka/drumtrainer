<?php
/**
 *  Extend User Profile
 * Create custom table
 */

function drumtrainer_tracker_table()
{
    global $wpdb;

    $table_name = $wpdb->prefix . "tracker";
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name)
    {
        $sql = 'CREATE TABLE ' . $table_name . "(
        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        `user_id` bigint(20) unsigned NOT NULL DEFAULT 0,
        `date` date NOT NULL DEFAULT '0000-00-00',
        `a_system` text COLLATE utf8mb4_unicode_520_ci,
        `b_system` text COLLATE utf8mb4_unicode_520_ci,
        `c_system` text COLLATE utf8mb4_unicode_520_ci,
        `d_system` text COLLATE utf8mb4_unicode_520_ci,
        `legs_system` text COLLATE utf8mb4_unicode_520_ci,
        `sport_system` text COLLATE utf8mb4_unicode_520_ci,
        `music_system` text COLLATE utf8mb4_unicode_520_ci,
        `meditation_system` text COLLATE utf8mb4_unicode_520_ci,
        `emotion` bigint(20) unsigned DEFAULT 0,
        `total_time` bigint(20) unsigned NOT NULL DEFAULT 0,
        PRIMARY KEY (id),
        KEY `user_id` (`user_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        add_option('tracker_database_version', '1.0');
    }
}

add_action( 'init', 'drumtrainer_tracker_table');


function drumtrainer_tracker_form()
{
    ob_start();
    get_template_part('template-parts/tracker/form');
    return ob_get_clean();
}

add_shortcode('drumtrainer_tracker_form', 'drumtrainer_tracker_form');



function drumtrainer_show_tracker_table()
{
    ob_start();
    get_template_part('template-parts/tracker/table');
    return ob_get_clean();
}

add_shortcode('drumtrainer_tracker_table', 'drumtrainer_show_tracker_table');


function drumtrainer_show_results()
{
    ob_start();
    get_template_part('template-parts/tracker/results');
    return ob_get_clean();
}

add_shortcode('drumtrainer_show_results', 'drumtrainer_show_results');


function drumtrainer_tracker_data_systems()
{
    $data = drumtrainer_get_tracker_data_systems();
    return json_encode($data);
}

add_shortcode('drumtrainer_tracker_data_systems', 'drumtrainer_tracker_data_systems');




function drumtrainer_set_tracker_data()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "tracker";
    $current_user = get_current_user_id();

    if(!current_user_can('mepr-active', 'membership: 575')) {
        return;
    }

   $data = array(
       'user_id' => $current_user,
       'date' => date('Y-m-d'),
       'a_system' => $_POST['tracker_system_A'],
       'b_system' => $_POST['tracker_system_B'],
       'c_system' => $_POST['tracker_system_C'],
       'd_system' => $_POST['tracker_system_D'],
       'legs_system' => $_POST['tracker_legs'],
       'sport_system' => $_POST['tracker_sport'],
       'music_system' => $_POST['tracker_music'],
       'meditation_system' => $_POST['tracker_meditation'],
       'emotion' => $_POST['tracker_emotion']
   );

   $totalMinutes = $data['a_system'] + $data['b_system'] + $data['c_system'] + $data['d_system'];

   $data['total_time'] = $totalMinutes;
   
	$oldData = drumtrainer_get_tracker_data_by_date();
	
	if($oldData === NULL)
    {
        if(isset($_POST['tracker_save'])) {
	        set_tracked_minutes($totalMinutes);
            $wpdb->insert($table_name, $data);
        }

    }
    else {
        if(isset($_POST['tracker_save'])) {
	        $difference = $totalMinutes - $oldData->total_time;
	        set_tracked_minutes($totalMinutes, $current_user, $difference);
	
	        $wpdb->update($table_name, $data, array('user_id' => $current_user, 'date' => date('Y-m-d ')));
        }
    }
	
}

function drumtrainer_get_tracker_data_by_date()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "tracker";

    $current_user = get_current_user_id();
    $data = $wpdb->get_row("SELECT * FROM $table_name WHERE user_id = $current_user AND DATE(`date`)=date_format(CURDATE(), '%Y-%m-%d') ");

    return $data;

}

function drumtrainer_get_tracker_data()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "tracker";

    $current_user = get_current_user_id();
    $data = $wpdb->get_results("SELECT * FROM $table_name WHERE user_id = $current_user ORDER BY `date` ASC");

    return $data;

}

function drumtrainer_get_tracker_data_systems($month = null, $year = null)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "tracker";

    if($month === null) {
        $month = date('m');
    }
    if($year === null) {
        $year = date('Y');
    }

    $current_user = get_current_user_id();
    $data = $wpdb->get_results($wpdb->prepare("SELECT `date`, `a_system`, `b_system`, `c_system`, `d_system`, `total_time` FROM $table_name WHERE user_id = %d AND MONTH(`date`) = %d AND YEAR(`date`) = %d ORDER BY `date` ASC", $current_user, $month, $year));

    return $data;
}

function drumtrainer_get_tracker_data_by_month($month = null, $year = null)
{
    global $wpdb;

    if($month === null) {
        $month = date('m');
    }
    if($year === null) {
        $year = date('Y');
    }

    $current_user = get_current_user_id();
    $data = $wpdb->get_results($wpdb->prepare("SELECT * FROM `wp_tracker` WHERE user_id = %d AND MONTH(`date`) = %d AND YEAR(`date`) = %d", $current_user, $month, $year));

    return $data;
}

function drumtrainer_get_total_time($month = null, $year = null, $onlyHours = 0) {
    global $wpdb;
    $table_name = $wpdb->prefix . "tracker";

    if($month === null) {
        $month = date('m');
    }
    if($year === null) {
        $year = date('Y');
    }

    $current_user = get_current_user_id();
    $data = $wpdb->get_var($wpdb->prepare("SELECT sum(`total_time`) FROM $table_name WHERE user_id = %d AND MONTH(`date`) = %d AND YEAR(`date`) = %d", $current_user, $month, $year));

    $totalTime = get_hours($data);

    if($onlyHours === 1) {
        return $totalTime['hours'];
    }

    return $totalTime['hours'] . ' ' . $totalTime['hoursText'] . ' ' . pll__('and') . ' ' . $totalTime['minutes'] . ' ' . $totalTime['minutesText'];

}

add_shortcode('get_tracker_total_time', 'drumtrainer_get_total_time');

function drumtrainer_get_total_hours()
{
    global $wpdb;
    $table_name = $wpdb->prefix . "tracker";

    $current_user = get_current_user_id();
    // $data = $wpdb->get_var($wpdb->prepare("SELECT sum(`total_time`) FROM $table_name WHERE user_id = %d", $current_user));
    $minutes = get_tracked_minutes();

    $totalTime = get_hours($minutes);

    return '<span class="total-hours-value">' . $totalTime['hours'] . '</span> ' . $totalTime['hoursText'] . ' ' . pll__('and') . ' ' . $totalTime['minutes'] . ' ' . $totalTime['minutesText'];
}

add_shortcode('get_tracker_total_hours', 'drumtrainer_get_total_hours');

function get_hours($data)
{
    $data = array(
        'hours' => intval($data / 60),
        'minutes' => $data % 60,
        'hoursText' => '',
        'minutesText' => ''
    );

    if($data['hours'] === 1)
    {
        $hoursText = pll__('hour'); // hodinu
    }
    elseif ($data['hours'] > 1 && $data['hours'] < 4)
    {
        $hoursText = pll__('hours(2-4)'); // hodiny
    }
    else {
        $hoursText = pll__('hours'); // hodin
    }

    if($data['minutes'] === 1)
    {
        $minutesText = pll__('minute'); // minuta
    }
    elseif ($data['minutes'] > 1 && $data['minutes'] < 5)
    {
        $minutesText = pll__('minutes(2-5)'); // minuty
    }
    else {
        $minutesText = pll__('minutes'); // minut
    }

    $data['hoursText'] = $hoursText;
    $data['minutesText'] = $minutesText;

    return $data;
}

function drumtrainer_show_rewards() {
	$args = array(
		'post_type' => 'product',
		'post_status' => 'publish',
		'tax_query'             => array(
			array(
				'taxonomy'      => 'product_cat',
				'field'         => 'term_id', //This is optional, as it defaults to 'term_id'
				'terms'         => 51,
				'operator'      => 'IN' // Possible values are 'IN', 'NOT IN', 'AND'.
			),
		)
	);
	
	$products = new WP_Query($args);
//	var_dump($products->posts);
	
	ob_start();
	set_query_var( 'products', $products->posts );
	get_template_part('template-parts/tracker/rewards');
	return ob_get_clean();
}

add_shortcode('show_rewards', 'drumtrainer_show_rewards');
