<?php

// add custom link into menu
function give_user_points(){
	$current_user = get_current_user_id();
	if($current_user) {
		$points = get_user_meta($current_user, 'points', true);
		if(!$points) {
			$points = 0;
		}
		return $points;
	}
	else {
		return false;
	}
	
}
add_shortcode('user_points', 'give_user_points');



function restrict_posts() {
	$args = array(
		'post_type' => 'post',
		'category__in' => array( 74, 52, 108, 118 ), // id of categories Smycky and Videa (CZ and EN)
	);
	
	$posts = get_posts($args);
	$postIds = array();
	
	foreach($posts as $post) {
		array_push($postIds, $post->ID);
	}
	
	$current_user = get_current_user_id();
	$restrict_posts = get_user_meta($current_user, 'restrict_posts', true);
	if(!$restrict_posts) {
		add_user_meta($current_user, 'restrict_posts', $postIds);
	}
	else {
		update_user_meta($current_user, 'restrict_posts', $postIds);
	}
}
// call after user login
add_action('wp_login', 'restrict_posts');

function add_to_unlock_posts($postId) {
	
	$current_user = get_current_user_id();
	$unlock_posts = get_user_meta($current_user, 'unlock_posts', true);
	$points = get_points();
	$postPoints = get_field('loop_points', $postId);
	
	if($points >= $postPoints) {
		if (empty($unlock_posts)) {
			add_user_meta($current_user, 'unlock_posts', array($postId));
		} else if ($unlock_posts === "") {
			update_user_meta($current_user, 'unlock_posts', $unlock_posts);
		} else {
			if (in_array($postId, $unlock_posts)) {
				return false;
			} else {
				array_push($unlock_posts, $postId);
				update_user_meta($current_user, 'unlock_posts', $unlock_posts);
			}
		}
	
		countdown_points($postPoints);?>
		<style>
			#post-<?php echo $postId; ?> .vimeo-content iframe {
				opacity: 1;
			}
		</style>
		<?php
	}
}

function get_unlock_posts() {
	$current_user = get_current_user_id();
	$posts = get_user_meta($current_user, 'unlock_posts', true);
	
	return $posts;
}

function add_points($value) {
	$current_user = get_current_user_id();
	$points = get_points();
	
	if(empty($points) && (int)$points !== 0) {
		add_user_meta($current_user, 'points', $value);
	}
	else if($points === "") {
		update_user_meta($current_user, 'points', $value);
	}
	else {
		if (in_array($value, $points)) {
			return;
		}
		else {
			update_user_meta($current_user, 'points', $points + $value);
		}
	}
}

function countdown_points($value) {
	$current_user = get_current_user_id();
	$points = get_user_meta($current_user, 'points', true);
	
	update_user_meta($current_user, 'points', $points-$value);
}

function get_points() {
	$current_user = get_current_user_id();
	$points = get_user_meta($current_user, 'points', true);
	
	return $points;
}

function lockVideo($postId, $postPoints, $isAllowed) {
	$localized_data = array(
		'post_id' => $postId,
		'post_points' => $postPoints,
		'template_url' => get_bloginfo('stylesheet_directory')
	); ?>
	<script>
        function addLockButtonToVideo($template_url, $postId, $postPoints){
            let videoItem = document.querySelector('.post-' + $postId + ' .video-shortcode');
            let isAllowed = <?php echo $isAllowed ?>;
			let pointsText = "<?php pll_e('points'); ?>";
            videoItem.style.opacity = 1;
            $videoPointsElement = document.createElement('DIV');
            $unlockIcon = document.createElement('IMG');
            $spanForText = document.createElement('SPAN');
            $loopElement = document.createElement('DIV');
            $loopLinkElement = document.createElement('A');

            $spanForText.classList.add('text');
            $loopElement.classList.add('loop-' + $postId);
            
            $unlockIcon.classList.add('ico');
            $loopLinkElement.classList.add('controls-unlock');
            
            $unlockIcon.src = $template_url + '/assets/img/lock_open.svg';

            $videoPointsElement.appendChild($loopElement);
            $loopElement.appendChild($loopLinkElement);

            $loopLinkElement.href = '';
            $loopLinkElement.id = 'post-' + $postId;
            $loopLinkElement.appendChild($unlockIcon);
            $loopLinkElement.appendChild($spanForText);
            videoItem.appendChild($videoPointsElement);

            $spanForText.innerHTML = '<span class="points-value">' + $postPoints + '</span> ' + pointsText;
            $videoPointsElement.classList.add('ico-text', 'video-points');
            if(!isAllowed) {
                $videoPointsElement.classList.add('not-allowed');
            }
        }
        
        addLockButtonToVideo("<?php echo $localized_data['template_url']; ?>", <?php echo $localized_data['post_id']; ?>, <?php echo $localized_data['post_points']; ?> );
	</script>
<?php

//	return true;
}

// set meta after subscription
function set_unlocked_after_subscription() {
	// set all post to restrict
	restrict_posts();
	
	$current_user = get_current_user_id();
	add_user_meta($current_user, 'points', 0);
	add_user_meta($current_user, 'tracked_minutes', 0);

}

add_action('mepr-event-subscription-created', 'set_unlocked_after_subscription');

function drumtrainer_add_points_order($orderId) {
	$current_user = get_current_user_id();
	$applied_orders = drumtrainer_get_points_orders();

	if (empty($applied_orders)) {
		add_user_meta($current_user, 'applied_orders', array($orderId));
	} else if ($unlock_posts === "") {
		update_user_meta($current_user, 'applied_orders', $applied_orders);
	} else {
		if (in_array($orderId, $applied_orders)) {
			return false;
		} else {
			array_push($applied_orders, $orderId);
			update_user_meta($current_user, 'applied_orders', $applied_orders);
		}
	}
}

function drumtrainer_get_points_orders() {
	$current_user = get_current_user_id();
	$applied_orders = get_user_meta($current_user, 'applied_orders', true);

	if(empty($applied_orders)) {
		return [];
	}

	return $applied_orders;
}

add_action( 'init', 'drumtrainer_current_user_orders' );

function drumtrainer_current_user_orders() {
    $customer_orders = get_posts( array(
        'numberposts' => -1,
        'meta_key'    => '_customer_user',
        'meta_value'  => get_current_user_id(),
        'post_type'   => wc_get_order_types(),
        'post_status' => array_keys( wc_get_order_statuses() ),
    ) );

    $points_posts = array(1941, 1944, 1945, 1946);

    foreach($customer_orders as $customer_order) {

		$order = wc_get_order( $customer_order->ID ); 
		$points_orders = drumtrainer_get_points_orders();

        if($order->get_status() === "completed" && !(in_array($order->get_id(), $points_orders))) {

            foreach ($order->get_items() as $item_id => $item_data) {
				$product = $item_data->get_product();

                if(in_array($product->get_id(), $points_posts)) {
                    $product_name = $product->get_name();

					$points = (int) filter_var($product_name, FILTER_SANITIZE_NUMBER_INT);
					add_points($points);
					drumtrainer_add_points_order($order->get_id());
                }
            }
        }
    }

}