<?php
/**
 *  Extend User Profile
 * Create custom table
 */

function drumtrainer_player_table()
{
    global $wpdb;

    $table_name = $wpdb->prefix . "player";
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name)
    {
        $sql = 'CREATE TABLE ' . $table_name . "(
        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        `user_id` bigint(20) unsigned NOT NULL DEFAULT '0',
        `nickname` text COLLATE utf8mb4_unicode_520_ci,
        `genre` text COLLATE utf8mb4_unicode_520_ci,
        `age` bigint(20) unsigned NOT NULL DEFAULT '0',
        `play_age` bigint(20) unsigned NOT NULL DEFAULT '0',
        `musicians` text COLLATE utf8mb4_unicode_520_ci,
        `musician_goal` text COLLATE utf8mb4_unicode_520_ci,
        `application_goal` text COLLATE utf8mb4_unicode_520_ci,
        `story` longtext COLLATE utf8mb4_unicode_520_ci,
        PRIMARY KEY (id),
        KEY `user_id` (`user_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;";

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        add_option('player_database_version', '1.0');
    }
}

add_action( 'init', 'drumtrainer_player_table');


function drumtrainer_player_form()
{
    $data = drumtrainer_get_player_data(get_current_user_id());
    
	ob_start();
	set_query_var( 'data', $data );
	get_template_part('template-parts/user/profile');
	
	return ob_get_clean();
}

add_shortcode('drumtrainer_player_form', 'drumtrainer_player_form');


function drumtrainer_set_player_data($user_id = null)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "player";
	$current_user = get_current_user_id();
	
	
	$data = array(
       'user_id' => $current_user,
       'nickname' => $_POST['player_profile_nickname'],
       'genre' => $_POST['player_profile_favorite_genre'],
       'age' => $_POST['player_profile_age'],
       'play_age' => $_POST['player_profile_play_age'],
       'musicians' => $_POST['player_profile_musicians'],
       'musician_goal' => $_POST['player_profile_musician_goal'],
       'application_goal' => $_POST['player_profile_application_goal'],
       'story' => $_POST['player_profile_story']
   );

    if(drumtrainer_get_player_data($user_id) === NULL)
    {
        if(isset($_POST['player_profile_save'])) {
            $wpdb->insert($table_name, $data);
        }

    }
    else {
        if(isset($_POST['player_profile_save'])) {
            $wpdb->update($table_name, $data, array('user_id' => $user_id));
        }
    }
}

function drumtrainer_get_player_data($user_id)
{
    global $wpdb;
    $table_name = $wpdb->prefix . "player";
    
    $user = $wpdb->get_row($wpdb->prepare("SELECT * FROM `wp_player` WHERE user_id = %d", $user_id));
//    var_dump($user_id);

    return $user;

}

