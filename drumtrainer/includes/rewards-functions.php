<?php

function set_want_reward($rewardId) {
	$current_user = get_current_user_id();
	$rewardValue = get_field('reward_hours', $rewardId);
	$trackedHours = get_hours(get_tracked_minutes());
	
	$want_reward = get_user_meta($current_user, 'want_reward', true);

	if($trackedHours >= $rewardValue) {
		if (empty($want_reward)) {
			add_user_meta($current_user, 'want_reward', array($rewardId));
		} else if ($want_reward === "") {
			update_user_meta($current_user, 'want_reward', $want_reward);
		} else {
			if (in_array($rewardId, $want_reward)) {
				return false;
			} else {
				array_push($want_reward, $rewardId);
				update_user_meta($current_user, 'want_reward', $want_reward);
			}
		}
		countdown_minutes($rewardValue * 60);
	}

}

function set_tracked_minutes($value, $userId = null, $difference = null) {

	if($userId === null) {
		$userId = get_current_user_id();
	}
	$trackedHours = intval(get_tracked_minutes($userId));

	if (empty($trackedHours) && $trackedHours !== 0) {
		add_user_meta($userId, 'tracked_minutes', $value);
	} else if ($trackedHours === 0) {
		update_user_meta($userId, 'tracked_minutes', ($value * 60));
	} else {
		if($difference !== null) {
			update_user_meta($userId, 'tracked_minutes', $trackedHours + $difference);
		}
		else {
			update_user_meta($userId, 'tracked_minutes', $trackedHours + ($value * 60));
		}
	}
}

function get_tracked_minutes($userId = null) {
	if($userId === null) {
		$userId = get_current_user_id();
	}
	$minutes = get_user_meta($userId, 'tracked_minutes', true);
	return $minutes;
}

function countdown_minutes($value) {
	$userId = get_current_user_id();
	$minutes = get_user_meta($userId, 'tracked_minutes', true);
	
	update_user_meta($userId, 'tracked_minutes', $minutes - $value);
}

function get_rewards($userId = null) {
	
	if($userId === null) {
		$userId = get_current_user_id();
	}
	
	$rewards = get_user_meta($userId, 'want_reward', true);
	
	return $rewards;
}

add_action( 'admin_menu', 'drumtrainer_admin_rewards_register' );

function delete_want_reward($rewardId, $userId) {
	
	$want_reward = get_user_meta($userId, 'want_reward', true);
	
	if (empty($want_reward) || $want_reward === "") {
		delete_user_meta($userId, 'want_reward');
	} else {
		if (($key = array_search($rewardId, $want_reward)) !== false) {
			unset($want_reward[$key]);
		}
		update_user_meta($userId, 'want_reward', $want_reward);

		if (empty($want_reward) || $want_reward === "") {
			delete_user_meta($userId, 'want_reward');
		}
	}
}

function drumtrainer_admin_rewards_register()
{
	add_menu_page(
		'Odměny',     // page title
		'Odměny',     // menu title
		'manage_options',   // capability
		'rewards',     // menu slug
		'drumtrainer_admin_page_rewards' // callback function
	);
}
function drumtrainer_admin_page_rewards()
{
	global $title;
	
	print '<div class="users-rewards">';
	print "<h1>$title</h1>";
	get_users_rewards_by_meta('want_reward');
	get_users_rewards_by_meta('gained_rewards');
	
	print '</div>';
}



function get_users_rewards_by_meta($metaKey) {
	$users = get_users(
		array(
			'meta_key' => $metaKey,
			'count_total' => true
		)
	);
	
	
	$title = 'Přehled žádostí';
	ob_start();
	
	if($metaKey === 'want_reward') {
		$title = 'Žádosti o odměny';
		set_query_var( 'type', 'want' );
		
	}
	if($metaKey === 'gained_rewards') {
		$title = 'Odeslané odměny';
		set_query_var( 'type', 'send' );
		
	}
	print $title;
	set_query_var( 'users', $users );
	get_template_part('template-parts/admin/rewards');
}

function get_gained_rewards() {
	$current_user = get_current_user_id();
	
	$rewards = get_user_meta($current_user, 'gained_rewards', true);
	
	return $rewards;
}

function set_gained_reward($rewardId, $userId = null) {
	if($userId === null) {
		$userId = get_current_user_id();
	}
	$gainedRewards = get_gained_rewards();
	
	if (empty($gainedRewards)) {
		add_user_meta($userId, 'gained_rewards', array($rewardId));
	} else if ($gainedRewards === "") {
		update_user_meta($userId, 'gained_rewards', $gainedRewards);
	} else {
		if (in_array($rewardId, $gainedRewards)) {
			return false;
		} else {
			array_push($gainedRewards, $rewardId);
			update_user_meta($userId, 'gained_rewards', $gainedRewards);
		}
	}
	
}
