<?php
/**
 *  Extend User Profile
 * Create custom table
 */

function drumtrainer_training_plans_table()
{
    global $wpdb;

    $table_name = $wpdb->prefix . "training_plans";
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name)
    {
        $sql = 'CREATE TABLE ' . $table_name . ' (
        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        `user_id` bigint(20) unsigned NOT NULL,
        `plan_name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
        `total_time` bigint(20) unsigned NOT NULL DEFAULT 0,
        `datetime` datetime NOT NULL DEFAULT \'0000-00-00 00:00:00\',
        `active` varchar(20) COLLATE utf8mb4_unicode_520_ci NOT NULL DEFAULT \'0\',
        `note` text COLLATE utf8mb4_unicode_520_ci,
        PRIMARY KEY (id),
        KEY `user_id` (`user_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci';

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        add_option('player_database_version', '1.0');
    }
}

add_action( 'init', 'drumtrainer_training_plans_table');

function drumtrainer_training_plan_systems()
{
    global $wpdb;

    $table_name = $wpdb->prefix . "training_plan_systems";
    if($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = 'CREATE TABLE ' . $table_name . ' (
        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        `plan_id` bigint(20) unsigned NOT NULL,
        `system_type` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
        `exercise_name` text COLLATE utf8mb4_unicode_520_ci,
        `total_time` bigint(20) unsigned NOT NULL DEFAULT 0,
        `bmp` bigint(20) unsigned NOT NULL DEFAULT 0,
        PRIMARY KEY (`id`),
        KEY `plan_id` (`plan_id`)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci;';

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        add_option('player_database_version', '1.0');
    }
}

add_action( 'init', 'drumtrainer_training_plan_systems');



function drumtrainer_training_plan_form()
{
    ob_start();
    get_template_part('template-parts/training-plan/form');
    return ob_get_clean();
}

add_shortcode('drumtrainer_training_plan_form', 'drumtrainer_training_plan_form');


function drumtrainer_training_plan_active()
{
    ob_start();
    get_template_part('template-parts/training-plan/table');
    return ob_get_clean();
}
add_shortcode('drumtrainer_training_plan_active', 'drumtrainer_training_plan_active');


function drumtrainer_set_training_plan_data($table_name)
{
    global $wpdb;
    $table_name = $wpdb->prefix . $table_name;
    $current_user = get_current_user_id();

    $trainingData = $_POST['training_form_systems_'];

   $plan_data = array(
       'user_id' => $current_user,
       'plan_name' => $_POST['training_form_plan_name'],
       'total_time' => $_POST['training_form_plan_total_time'],
       'datetime' => date('Y-m-d H:i:s'),
       'note' => $_POST['training_form_systems_']['d-system'][0]['note']
   );

   if(isset($_POST['player_plan_save'])) {
		$wpdb->insert('wp_training_plans', $plan_data);
	}

   $lastid = $wpdb->insert_id;
    foreach($trainingData as $data) {
	    foreach ($data as $row) {
            if($row['exercise'] !== '' && $row['time'] !== '') {
                $system_data = array(
                   'plan_id' => $lastid,
                   'system_type' => $row['name'],
                   'exercise_name' => $row['exercise'],
                   'total_time' => $row['time'],
                   'bmp' => $row['bmp']
               );

                 if(isset($_POST['player_plan_save'])) {
                     $wpdb->insert($table_name, $system_data);
                 }
            }
            else {
                continue;
            }
        }
    }


//   $plan_data = array(
//       'user_id' => $current_user,
//       'plan_name' => $_POST['exercise'],
//       'total_time' => $_POST['time'],
//       'datetime' => date('Y-m-d H:i:s'),
//   );
//
//   var_dump($_POST);
//
//   $system_data = array(
//       'plan_id' => $plan_id,
//       'system_type' => 'A-system',
//       'exercise_name' => $_POST['player_plan_systemA-exercise-1'],
//       'total_time' => $_POST['player_plan_systemA-time-1'],
//   );

//   $data = $plan_data;

//   if($plan_id !== null)
//   {
//       $data = $system_data;
//   }

//    if(drumtrainer_get_training_plan_data($table_name) === NULL)
//    {


//    }
//    else {
//        if(isset($_POST['player_plan_save'])) {
//            $wpdb->update($table_name, $data, array('user_id' => $current_user));
//        }
//    }

}

function drumtrainer_get_training_plan_data($table_name, $plan_id = null, $system_type = null)
{
    global $wpdb;

    if(strpos($table_name, $wpdb->prefix) === false)
    {
        $table_name = $wpdb->prefix . $table_name;
    }

    $current_user = get_current_user_id();
    $active = 1;

    if($plan_id !== null)
    {
        $data = $wpdb->get_results("SELECT * FROM $table_name WHERE plan_id = $plan_id");
    }

    else if($system_type !== null)
    {
        $data = $wpdb->get_row("SELECT * FROM $table_name WHERE plan_id = $plan_id AND system_type = $system_type");
    }

    else {
    	if($table_name !== 'wp_training_plan_systems') {
		    $data = $wpdb->get_row("SELECT * FROM $table_name WHERE user_id = $current_user AND active = $active");
	    }
    	else {
		    $data = $wpdb->get_row("SELECT * FROM $table_name");
	    }
    }
    return $data;
}

function drumtrainer_get_systems()
{
	global $wpdb;
	$data = $wpdb->get_results("SELECT * FROM `wp_training_systems`");

	return $data;
}

function drumtrainer_get_plans()
{
	global $wpdb;
	$current_user = get_current_user_id();
	
	$data = $wpdb->get_results("SELECT * FROM `wp_training_plans` WHERE user_id = $current_user");

	return $data;
}

function drumtrainer_set_active_plan()
{
	global $wpdb;
	$current_user = get_current_user_id();

	$activePlan = $wpdb->get_results("SELECT * FROM `wp_training_plans` WHERE user_id = $current_user AND active = 1");
	$data = array('active' => 0);

	foreach($activePlan as $plan) {
		$wpdb->update('wp_training_plans', $data, array('id' => $plan->id));
	}

	$planId = (int)$_POST['active_plan'];
	$data = array('active' => 1);

	$wpdb->update('wp_training_plans', $data, array('id' => $planId));
}

function drumtrainer_get_select_plan_form()
{
	ob_start();
    get_template_part('template-parts/training-plan/select', 'form');
    return ob_get_clean();
}
add_shortcode('drumtrainer_get_select_plan_form', 'drumtrainer_get_select_plan_form');
