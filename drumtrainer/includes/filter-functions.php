<?php

add_action('wp_ajax_myfilter', 'drumtrainer_filter_function'); // wp_ajax_{ACTION HERE}
add_action('wp_ajax_nopriv_myfilter', 'drumtrainer_filter_function');

function drumtrainer_filter_function(){
	$accompanimentFilter = $_POST['accompanimentFilter'] !== '' ? $_POST['accompanimentFilter'] : '';
	$genreFilter = $_POST['genreFilter'] !== '' ? $_POST['genreFilter'] : '';
	$subcategorySlug = $_POST['subcategorySlug'] !== '' ? $_POST['subcategorySlug'] : '';
	
	$args = array(
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order'	=> 'DESC',
		'tax_query' => array(
			'relation' => 'AND',
			array(
				'taxonomy' => 'category',
				'field' => 'slug',
				'terms' => array( $subcategorySlug )
			),
			array(
				'relation' => 'AND',
				array(
					'taxonomy' => 'post_tag',
					'field'    => 'term_id',
					'terms'    => $accompanimentFilter,
				),
				array(
					'taxonomy' => 'post_tag',
					'field'    => 'term_id',
					'terms'    => $genreFilter,
				),
			)
		)
	);
	
	if( $accompanimentFilter === "" || $genreFilter === "" ) {
		$args['tax_query'][1]['relation'] = 'OR';
	}
	
	if( $accompanimentFilter === "" && $genreFilter === "" ) {
		$args['tax_query']['relation'] = 'OR';
	}
	
	
	$query = new WP_Query( $args );
	
	if( $query->have_posts() ) :
		while( $query->have_posts() ): $query->the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
            <?php echo fusion_render_rich_snippets_for_pages(); // WPCS: XSS ok. ?>
            <?php avada_featured_images_for_pages(); ?>
            <div class="post-content">
				<div class="loop-item">
	                <p class="title"><?php the_title()?></p>
					<div class="loop-content" style="display: block; opacity:0;"><?php the_content() ?></div>
					<div class="loop loop-<?php echo get_the_ID(); ?>">
		
					<?php if(in_array(get_the_ID(), get_unlock_posts()) || get_field('loop_public') === true) { ?>
						<a href="#/" class="controls controls-resume"><span class="icon icon-resume"><i class="fas fa-undo"></i></span></a>
						<a href="#/" class="controls controls-play"><span class="icon icon-play"></span></a>
						<a <?php echo get_field('loop_zip') !== NULL ? 'href="'. get_field('loop_zip') . '"' : ''?> class="controls controls-download <?php echo get_field('loop_zip') === NULL ? 'no-download' : ''; ?>">
	                        <span class="icon icon-download">
	                            <svg xmlns="http://www.w3.org/2000/svg" width="14" height="17" viewBox="0 0 14 17"><g><g><path fill="#bfc8cc" d="M0 15.065v2.009h14.06v-2.009zm14.06-9.039h-4.017V0H4.017v6.026H0l7.03 7.03z"/></g></g></svg>
	                        </span>
						</a>
					<?php }
					else { ?>
						<a href="#/" id="post-<?php echo get_the_ID(); ?>" class="controls controls-unlock controls-unlock-loop <?php echo get_points() < get_field('loop_points') ? 'not-allowed' : ''?>">
	                        <span class="ico-text">
		                        <svg xmlns="http://www.w3.org/2000/svg" width="48" height="48" class="ico" viewBox="0 0 48 48">
								    <path d="M0 0h48v48h-48z" fill="none"/>
								    <path d="M24 34c2.21 0 4-1.79 4-4s-1.79-4-4-4-4 1.79-4 4 1.79 4 4 4zm12-18h-2v-4c0-5.52-4.48-10-10-10s-10 4.48-10 10h3.8c0-3.42 2.78-6.2 6.2-6.2 3.42 0 6.2 2.78 6.2 6.2v4h-18.2c-2.21 0-4 1.79-4 4v20c0 2.21 1.79 4 4 4h24c2.21 0 4-1.79 4-4v-20c0-2.21-1.79-4-4-4zm0 24h-24v-20h24v20z"/>
								</svg>
	                            <span class="text"><span class="points-value"> <?php echo get_field('loop_points');?></span> <?php pll_e('points'); ?></span>
	                        </span>
						</a>
					<?php }?>
					</div>
				</div>
            </div>
		</div>
		<?php
		endwhile;
		wp_reset_postdata();
	else : ?>
		<p>Žádné příspěvky</p>
	<?php
	endif;
	
	die();
}
