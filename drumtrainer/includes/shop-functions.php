<?php

/* 100% width Woocommerce shop archive */
function woohundredwidth_is_hundred_percent_template($value) {
    if (is_shop() || is_product_category()){
        $value = true;
    }
return $value;
}
add_filter( 'fusion_is_hundred_percent_template', 'woohundredwidth_is_hundred_percent_template', 11, 2 );


// remove product description from single product pages
function wc_remove_description_tab( $tabs ) {
    if ( isset( $tabs['description'] ) ) {
        unset( $tabs['description'] );
    }
}
add_filter( 'woocommerce_product_tabs', 'wc_remove_description_tab', 11, 1 );
