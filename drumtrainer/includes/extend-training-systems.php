<?php

function drumtrainer_training_systems_table()
{
    global $wpdb;

    $table_name = $wpdb->prefix . "training_systems";
    if ($wpdb->get_var("SHOW TABLES LIKE '$table_name'") != $table_name) {
        $sql = 'CREATE TABLE ' . $table_name . ' (
        `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
        `nicename` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
        `class` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
        `name` varchar(191) COLLATE utf8mb4_unicode_520_ci NOT NULL,
        PRIMARY KEY (id)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_520_ci';

        require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
        dbDelta($sql);

        add_option('player_database_version', '1.0');

        $wpdb->insert('wp_training_systems', array('nicename' => 'A system', 'class' => 'a-system', 'name' => 'A-system'));
        $wpdb->insert('wp_training_systems', array('nicename' => 'B system', 'class' => 'b-system', 'name' => 'B-system'));
        $wpdb->insert('wp_training_systems', array('nicename' => 'C system', 'class' => 'c-system', 'name' => 'C-system'));
        $wpdb->insert('wp_training_systems', array('nicename' => 'D system', 'class' => 'd-system', 'name' => 'D-system'));
    }
}

add_action( 'init', 'drumtrainer_training_systems_table');
